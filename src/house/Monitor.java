package house;

import java.awt.RenderingHints.Key;
import java.util.Map.Entry;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

public class Monitor {
	
	public static String FILEPATH = new String("F:/Workspace/SmartHouse/src/Activities.txt");
	
	private ArrayList<MonitoredData> data,dataAux;
	private List<String> fileData;
	private HashMap<String,Integer> actionCountMap;
	private Set <HashMap.Entry<String,Integer>> actionCountMapSet;
	
	public Monitor() {
		data = new ArrayList<MonitoredData>();
		try {
			fileData = Files.readAllLines(Paths.get(FILEPATH));
		} catch (IOException e) {
			System.out.println("The files could not be read.");
			e.printStackTrace();
		}
		for (String line: fileData) {
			data.add(new MonitoredData(line));
		}
		
		
		actionCountMap = new HashMap<String, Integer>();
		actionCountMapSet = actionCountMap.entrySet();
		String currentAction = null;
		Integer currentActionCount = 0;
		dataAux = new ArrayList<MonitoredData>(data);
		for (MonitoredData m: data) {
			currentActionCount = 0;
			currentAction = m.getAction();
			if (!actionCountMap.containsKey(currentAction)) {
				for (MonitoredData m2: dataAux) {
					if (m2.getAction().equals(currentAction)) {
						currentActionCount++;
					}
				}
				actionCountMap.put(currentAction, currentActionCount);
			}
		}
		try{
		    PrintWriter writer = new PrintWriter("Activity_count.txt", "UTF-8");
		    for (Entry<String, Integer> k: actionCountMapSet) {
			    writer.println(k.getKey() + " " + k.getValue());
			}
		    writer.close();
		} catch (IOException e) {
			for (Entry<String, Integer> k: actionCountMapSet) {
			    System.out.println(k.getKey() + k.getValue());
			}
		}
	}
	
	
	
	public void printActionCount() {
		for (Entry<String, Integer> k: actionCountMapSet) {
			System.out.println(k.getKey() + ": " + k.getValue().toString());
		}
	}
	
	public void shortActivites() {
		ArrayList<String> a = new ArrayList<String>();
		for (Entry e: actionCountMapSet) {
			String activity = (String) e.getKey();
			Integer activityShortOcurrence = 0;
			for (MonitoredData m: data) {
				if (activity.equals(m.getAction()) && (m.timeDiff() < 300)) {
					activityShortOcurrence++;
				}
			}
			if (activityShortOcurrence > (Integer)e.getValue()*0.9) {
				a.add(activity);
			}
		}
		try{
		    PrintWriter writer = new PrintWriter("Short_Activites.txt", "UTF-8");
		    for (String s : a) {
			    writer.println(s);
			}
		    writer.close();
		} catch (IOException e) {
			for (String s : a) {
				System.out.println(s);
			}
		}
		
	}
	
	public void printData() {
		for (MonitoredData m: data) {
			m.print();
		}
	}
	
	public Integer distinctDays() {
		Integer distinctDays = 0;
		Time nextTime  = new Time();
		for (MonitoredData m: data) {
			if (m.getStartTime().compareDate(nextTime) == 1) {
				distinctDays++;
				nextTime = m.getStartTime();
			}
			if (m.getEndTime().compareDate(nextTime) == 1){
				distinctDays++;
				nextTime = m.getEndTime();
			}
		
		}
		return distinctDays;
	}
	
	public static void main(String[] args) {
		Monitor m = new Monitor();
		//m.printData();
		System.out.println(m.distinctDays());
		m.printActionCount();
		m.shortActivites();
	}
	
	
}
