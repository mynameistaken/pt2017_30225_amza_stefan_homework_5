package house;

public class Time {
	private Integer year;
	private Integer month;
	private Integer day;
	private Integer hour;
	private Integer minute;
	private Integer second;
	
	public Time() {
		year=0;
		month=0;
		day=0;
		hour=0;
		minute=0;
		second=0;
	}
	
	public Time(int y, int mo, int d, int h, int m, int s) {
		year=y;
		month=mo;
		day=d;
		hour=h;
		minute=m;
		second=s;
	}
	
	public void setDate(Integer y, Integer m, Integer d) {
		year=y;
		month=m;
		day=d;
	}
	
	public void setTime(Integer h, Integer m, Integer s) {
		hour=h;
		minute=m;
		second=s;
	}

	public Integer getYear() {
		return year;
	}

	public Integer getMonth() {
		return month;
	}

	public Integer getDay() {
		return day;
	}

	public Integer getHour() {
		return hour;
	}

	public Integer getMinute() {
		return minute;
	}

	public Integer getSecond() {
		return second;
	}
	
	public String dateString() {
		return year.toString() +"-"+ month.toString() +"-"+ day.toString();
	}
	public String timeString() {
		return hour.toString() +":"+ minute.toString() +":"+ second.toString();
	}
	
	public Boolean daysEqual(Time t) {
		Boolean b=false;
		if (this.year == t.getYear() && this.month == t.getMonth() && this.day == t.getDay()) {
			b=true;
		}
		return b;
	}
	
	public int compareDate(Time t) {
		if (t.getYear() < this.year) {
			return 1;
		}
		if (t.getMonth() < this.month) {
			return 1;
		}
		if (t.getDay() < this.day) {
			return 1;
		}
		if (t.getDay() == this.day && t.getMonth() == this.month && t.getYear() == this.year) return 0;
		return -1;
	}
	
	

}
