package house;

public class MonitoredData {
	private Time startTime, endTime;
	private String action;
	
	public MonitoredData(String input1) {
		startTime = new Time();
		endTime = new Time();
		String[] input,dateStart,timeStart,dateEnd,timeEnd,aux1,aux2;
		input = input1.split("		");
		aux1 = input[0].split(" ");
		aux2 = input[1].split(" ");
		dateStart = aux1[0].split("-");
		timeStart = aux1[1].split(":");
		dateEnd = aux2[0].split("-");
		timeEnd = aux2[1].split(":");
		
		startTime.setDate(Integer.parseInt(dateStart[0].replaceAll("\\s","")),Integer.parseInt(dateStart[1].replaceAll("\\s","")),Integer.parseInt(dateStart[2].replaceAll("\\s","")));
		startTime.setTime(Integer.parseInt(timeStart[0].replaceAll("\\s","")),Integer.parseInt(timeStart[1].replaceAll("\\s","")),Integer.parseInt(timeStart[2].replaceAll("\\s","")));
		startTime.setDate(Integer.parseInt(dateStart[0].replaceAll("\\s","")),Integer.parseInt(dateStart[1].replaceAll("\\s","")),Integer.parseInt(dateStart[2].replaceAll("\\s","")));
		startTime.setTime(Integer.parseInt(timeStart[0].replaceAll("\\s","")),Integer.parseInt(timeStart[1].replaceAll("\\s","")),Integer.parseInt(timeStart[2].replaceAll("\\s","")));
		endTime.setDate(Integer.parseInt(dateEnd[0].replaceAll("\\s","")),Integer.parseInt(dateEnd[1].replaceAll("\\s","")),Integer.parseInt(dateEnd[2].replaceAll("\\s","")));
		endTime.setTime(Integer.parseInt(timeEnd[0].replaceAll("\\s","")),Integer.parseInt(timeEnd[1].replaceAll("\\s","")),Integer.parseInt(timeEnd[2].replaceAll("\\s","")));
		endTime.setDate(Integer.parseInt(dateEnd[0].replaceAll("\\s","")),Integer.parseInt(dateEnd[1].replaceAll("\\s","")),Integer.parseInt(dateEnd[2].replaceAll("\\s","")));
		endTime.setTime(Integer.parseInt(timeEnd[0].replaceAll("\\s","")),Integer.parseInt(timeEnd[1].replaceAll("\\s","")),Integer.parseInt(timeEnd[2].replaceAll("\\s","")));
		
		action = input[2].replaceAll("\\s","");
	}
	
	public void print() {
		System.out.println("startTime: " + startTime.dateString() + " " + startTime.timeString() + "\nendTime: " + endTime.dateString() + " " + endTime.timeString() + "\nAction: " + action + "\n");
	}
	
	public Time getStartTime(){
		return startTime;
	}
	
	public Time getEndTime(){
		return endTime;
	}
	
	public String getAction(){
		return action;
	}
	
	public int timeDiff() {
		Time e = this.endTime;
		Time s = this.startTime;
		int secdiff = e.getSecond() - s.getSecond();
		int mindiff = e.getMinute() - s.getMinute();
		int hourdiff = e.getHour() - s.getHour();
		
		if (hourdiff<0) {
			hourdiff += 23;
		}
		
		if (mindiff<0) {
			mindiff += 59;
		}
		
		if (secdiff<0) {
			secdiff += 59;
		}
		
		return secdiff + 60*mindiff + 3600*hourdiff;
	}
	
}
